%% Start variables
clc;
clear variables;
close all force;
a=-5;
b=5;
e=10^(-3);
Nmax=10^6;

%% ����� �����������
method_vec = {'Newton'};
[min_f_vec(1,1),min_x_vec,N_vec(1,1),f2]=lab_optimal_vec('Newton',Nmax,e);
min_x1_vec(1,1)=min_x_vec(1);
min_x2_vec(1,1)=min_x_vec(2);
min_x3_vec(1,1)=min_x_vec(3);

table(method_vec,min_x1_vec,min_x2_vec,min_x3_vec,min_f_vec)



%% lab_optimal_vec(method,Nmax,e)
function [min_f,x,N,f] = lab_optimal_vec(method, Nmax, e)
    N=0;
    x=[-28;-21;-46]+e; % ��������� �����
    f=@(x) (x(1)+49+x(2)).^4 + (x(1)+78+x(3)).^2 + (x(2)+67+x(3)).^4; % �������� �������
   
    df=max([abs(fd(x,1)), abs(fd(x,2)), abs(fd(x,3))]);
    while (df>=e)&&(N<=Nmax)
        proiz=NaN(length(x),1);
        for i=1:length(x)
            proiz(i,1)=fd(x,i);
        end
        hess = hessian(x);
        H=hess^(-1);
        x_next=x-H*proiz;
        df=max([abs(fd(x,1)), abs(fd(x,2)), abs(fd(x,3))]);
        x=x_next;
        N=N+1;
    end
        
    min_f=f(x);


    
% ����������� ������� �������
    function diff = fd(x,i)
        h=10^(-6);
        x1=x;
        x1(i)=x1(i)-2*h;
        x2=x;
        x2(i)=x2(i)-h;
%         x3=x;
        x4=x;
        x4(i)=x4(i)+h;
        x5=x;
        x5(i)=x5(i)+2*h;
        diff=(f(x1)-8*f(x2)+8*f(x4)-f(x5))/(12*h);
    end

% ����������� ������� �������
    function diff2=fdd(x,i,j)
        h=10^-6;
        x1=x;
        x1(i)=x(i)+h;
        x2=x;
        diff2=(fd(x1,j)-fd(x2,j))/h;
    end

% �������
    function hes= hessian(x)
        hes=NaN(length(x));
        for i_hes=1:length(x)
            for j_hes=1:length(x)
                hes(i_hes,j_hes)=fdd(x,i_hes,j_hes);
            end
        end
    end
end
