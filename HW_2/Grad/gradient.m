%% Start variables
clc;
clear variables;
close all force;
a=-5;
b=5;
e=10^(-3);
Nmax=10^6;

%% ����� �����������
method_vec = {'Grad'};
[min_f_vec(1,1),min_x_vec,N_vec(1,1),f2]=lab_optimal_vec('Grad',Nmax,e);
min_x1_vec(1,1)=min_x_vec(1);
min_x2_vec(1,1)=min_x_vec(2);
table(method_vec,min_x1_vec,min_x2_vec,min_f_vec)


%% subplot(2,1,2)
s2=subplot(1,1,1);
X1=-50:1:50;
X2=-100:1:0;

Z=NaN(length(X2),length(X1));
for i=1:length(X1)
   for j=1:length(X2) 
       Z(j,i)=f2([X1(i);X2(j)]);
   end
end
[X1,X2]=meshgrid(X1,X2);

srf=surf(X1,X2,Z);
srf.EdgeColor='none';
srf.FaceAlpha=0.5;
colormap(s2,winter);
hold on;
scatter3(min_x1_vec(1),min_x2_vec(1),min_f_vec(1),'ok')
hold off

s2.Title.String='������� ����������� �������';
xlabel(s2,'x_{1}');
ylabel(s2,'x_{2}');
zlabel('f(x)');
legend(s2,'�����������','����������� �����');



%% lab_optimal_vec(method,Nmax,e)
function [min_f,x,N,f] = lab_optimal_vec(method, Nmax, e)
    N=0;
    x=[8;-45]; % ��������� �����
    f=@(x) 6*(x(2)+72-x(1).^2) + (40-x(1)).^2 + 7*x(1).^2; % �������� �������


    df=min(abs(fd(x,1)),abs(fd(x,2)));
    while (df>=e)&&(N<=Nmax)
        x_next=NaN(length(x),1);
        for i=1:length(x)
            x_next(i)=x(i)-lmin(x,i)*fd(x,i);
        end
        df=min(abs(fd(x,1)),abs(fd(x,2)));
        x=x_next;
        N=N+1;
    end
    min_f=f(x);

    
% ����������� ������� �������
    function diff = fd(x,i)
        h=10^(-6);
        x1=x;
        x1(i)=x1(i)-2*h;
        x2=x;
        x2(i)=x2(i)-h;
%         x3=x;
        x4=x;
        x4(i)=x4(i)+h;
        x5=x;
        x5(i)=x5(i)+2*h;
        diff=(f(x1)-8*f(x2)+8*f(x4)-f(x5))/(12*h);
    end


% ������� ��������������� ������
    function min_l= lmin(x,i)
       a=0;
       b=2;
       while abs(b-a)>e
           min_l=(b+a)/2;
           dl=(b-a)/2;
           x_a=x;
           x_a(i)=x(i)-(min_l-dl)*fd(x,i);
           x_b=x;
           x_b(i)=x(i)-(min_l+dl)*fd(x,i);
           if f(x_a)>=f(x_b)
               a=min_l;
           else
               b=min_l;
           end
       end
    end
end
